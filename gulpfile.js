var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');


gulp.task('css', function(){
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/css'))
});

gulp.task('watch',function(){
    gulp.watch('src/scss/**/*.scss',['css']);
});

gulp.task('default', [ 'css' ]);